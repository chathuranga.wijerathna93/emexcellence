public class Rover {

    private final int gridMaxX;
    private final int gridMaxY;

    Position position;

    public Rover(int gridMaxX, int gridMaxY) {
        this.gridMaxX = gridMaxX;
        this.gridMaxY = gridMaxY;
    }

    public void setDeploymentPosition(int x, int y, char facing) {

        if (x > gridMaxX || y > gridMaxY) {
            throw new RuntimeException("Error..! Can not Position the Rover");
        }

        position = new Position(x, y, facing);
    }

    public void getPosition() {
        System.out.println(position.x + " " + position.y + " " + position.facing);
    }

    public void navigate(String command) {
        if(position == null){
            throw new RuntimeException("Error..! Please Set the Deployment Position of the Rover");
        }

        for (int i = 0; i < command.length(); i++) {
            if (command.charAt(i) == 'L') {
                turnLeft();
            } else if (command.charAt(i) == 'R') {
                turnRight();
            } else if (command.charAt(i) == 'M') {
                move();
            } else {
                throw new IllegalArgumentException("Error..! Invalid Command");
            }
        }
    }

    private void move() {
        if (position.facing == 'N' && position.y < gridMaxY) {
            position.y++;
        } else if (position.facing == 'S' && position.y > 0) {
            position.y--;
        } else if (position.facing == 'E' && position.x < gridMaxX) {
            position.x++;
        } else if (position.facing == 'W' && position.x > 0) {
            position.x--;
        } else {
            System.out.println("Avoiding the Command.. Terrain Capacity Exceeded !");
        }
    }

    private void turnLeft() {
        if (position.facing == 'N') {
            position.facing = 'W';
        } else if (position.facing == 'W') {
            position.facing = 'S';
        } else if (position.facing == 'S') {
            position.facing = 'E';
        } else if (position.facing == 'E') {
            position.facing = 'N';
        }
    }

    private void turnRight() {
        if (position.facing == 'N') {
            position.facing = 'E';
        } else if (position.facing == 'E') {
            position.facing = 'S';
        } else if (position.facing == 'S') {
            position.facing = 'W';
        } else if (position.facing == 'W') {
            position.facing = 'N';
        }
    }

    static class Position {
        int x, y;
        char facing;

        Position(int x, int y, char facing) {
            this.x = x;
            this.y = y;
            this.facing = facing;
        }
    }
}
