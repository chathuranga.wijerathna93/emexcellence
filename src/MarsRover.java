public class MarsRover {

    public static void main(String[] args) {

        Rover rover = new Rover(5, 5);

        rover.setDeploymentPosition(1, 2, 'N');
        rover.navigate("LMLMLMLMM");
        rover.getPosition();

        rover.setDeploymentPosition(3, 3, 'E');
        rover.navigate("MMRMMRMRRM");
        rover.getPosition();
    }
}
